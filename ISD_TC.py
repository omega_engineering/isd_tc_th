import os, datetime
from enum import Enum
import time
from mte_packets import AsciiDevice
from mte_devices import *
from threading import Timer
import winsound
import ctypes  # An included library with Python install.
def Mbox(title, text, style):
    sty= int(style) + 4096
    return ctypes.windll.user32.MessageBoxW(0, text, title, sty)


sequence_prompt =[mte.SelectionControl(items=['Full Test','Cal Only','Functional Only'])]

model_prompt = [mte.SelectionControl(items=['DP41-']),
                mte.SelectionControl(items=['U_Type-', 'P_Type-', 'T_Type-','S_Type-']),
                mte.SelectionControl(items=['AC','DC'])]

settings_path       = os.path.dirname(os.path.realpath(__file__)) + "\script_settings.xml"
switch_table_pathA   = os.path.dirname(os.path.realpath(__file__)) + "\SwitchTableA.xml"


UUT_RS232_COM_PORT = mte.GetScriptParam(settings_path, 'UUT_RS232_COM_PORT')

print(UUT_RS232_COM_PORT)

SW_REV = "v010418"

SOUND_ENABLE = 1

class BEEP:
    enable =0




class Model:
    Type = 0 # 0 = UR 1= PR 2= TR 3= SR
    UniversalMode="D739ED"
    TemperatureMode="D739C0"
    ProcessMode="D73969"
    StrainMode="D7393D"
    AC_Power = False



class ISD_TCSwitch (Enum):
    POWER = "POWER"
    TC1="TC1"
    TC2="TC2"
    TC1N="TC1N"
    TC2N="TC2N"
    COMP1="COMP1"
    COMP2="COMP2"




class Cmd(Enum):
    Process_Reading             =   0x01
    Cal_Process_Reading         =   0x01
    Cal_Cjc_Reading             =   0x02
    Cal_Strain_Reading          =   0x02
    Input_Type                  =   0x07
    Reading_Config              =   0x08
    Analog_Out_1_Config         =   0x09
    Analog_Out_2_Config         =   0x0A
    Analog_Volt_Out_Offset      =   0x4F
    Analog_Volt_Out_Gain        =   0x50
    Analog_Current_Out_Offset   =   0x51
    Analog_Current_Out_Gain     =   0x52
    Serial_Number_Days          =   0x58
    Serial_Number_Seconds       =   0x59

    Cal_Strain_Offset           =   0x45
    Cal_10V_Offset              =   0x54
    Cal_1V_Offset               =   0x53
    Cal_100mV_Offset            =   0x40
    Cal_100_Ohm_Offset          =   0x42
    Cal_Cjc_Offset              =   0x47
    Cal_20mA_Offset             =   0x41
    Cal_10V_Gain                =   0x56
    Cal_1V_Gain                 =   0x55
    Cal_100mV_Gain              =   0x43
    Cal_100_Ohm_Gain            =   0x45    # for strain
    Cal_Cjc_Gain                =   0x46    # for TC
    Cal_20mA_Gain               =   0x44
    Cal_RTD_100_Gain            =   0x48    # for RTD 100
    Cal_RTD_1K_Gain             =   0x49    # for RTD 1000
    Cal_1P25V_Ref               =   0x57

    Reading_Offset              =   0x03
    Reading_Scale               =   0x14



R = 'R'
W = 'W'
X = 'X'
P = 'P'
G = 'G'

hex_ascii = lambda x: int(x, 16)
signed_hex = 0
unsigned_hex = 1

class iSeriesDevice(AsciiDevice):
    protocol = {
        Cmd.Process_Reading             :   {'data len': 0, 'class': [X],    'data': float},
        Cmd.Cal_Process_Reading         :   {'data len': 0, 'class': [X],    'data': float},
        Cmd.Cal_Cjc_Reading             :   {'data len': 0, 'class': [X],    'data': float},
        Cmd.Cal_Strain_Reading          :   {'data len': 0, 'class': [X],    'data': float},
        Cmd.Reading_Scale               :   {'data len': 6, 'class': [P],    'data': hex},
        Cmd.Reading_Offset              :   {'data len': 6, 'class': [P],    'data': hex},
        Cmd.Reading_Config              :   {'data len': 2, 'class': [R, W], 'data': hex},
        Cmd.Input_Type                  :   {'data len': 2, 'class': [R, W], 'data': hex     },
        Cmd.Analog_Out_1_Config         :   {'data len': 2, 'class': [R, W], 'data': hex     },
        Cmd.Analog_Out_2_Config         :   {'data len': 2, 'class': [R, W], 'data': hex    },
        Cmd.Serial_Number_Days          :   {'data len': 4, 'class': [R, W], 'data': hex    },
        Cmd.Serial_Number_Seconds       :   {'data len': 4, 'class': [R, W], 'data': hex    },

        Cmd.Analog_Volt_Out_Offset      :   {'data len': 4, 'class': [R, W], 'data': hex     },
        Cmd.Analog_Current_Out_Offset   :   {'data len': 4, 'class': [R, W], 'data': hex     },
        Cmd.Analog_Current_Out_Gain     :   {'data len': 4, 'class': [R, W], 'data': hex     },

        Cmd.Cal_Strain_Offset           :   {'data len': 4, 'class': [R, W], 'data': hex    },
        Cmd.Cal_10V_Offset              :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_1V_Offset               :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_100mV_Offset            :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_100_Ohm_Offset          :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_Cjc_Offset              :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_20mA_Offset             :   {'data len': 4, 'class': [R, W], 'data': hex    },#
        Cmd.Cal_10V_Gain                :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_1V_Gain                 :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_100mV_Gain              :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_100_Ohm_Gain            :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_Cjc_Gain                :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_20mA_Gain               :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_RTD_100_Gain            :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_RTD_1K_Gain             :   {'data len': 4, 'class': [R, W], 'data': hex    },##
        Cmd.Cal_1P25V_Ref               :   {'data len': 4, 'class': [R, W], 'data': hex    },
    }
    error_response = {
        '?43'   :   'COMMAND ERROR',
        '?46'   :   'FORMAT ERROR',
        '?4C'   :   'CALIBRATION WRITE/LOCKOUT',
        '?45'   :   'EEPROM WRITE LOCKOUT',
        '?56'   :   'DECIMAL ERROR'
    }
    def __init__(self, handle):
        super().__init__(handle)
        self.setup(rx_start_of_frame='\r',rx_end='\n')
        self.debug = False
    def read(self, cmd:Cmd):
        if R in self.protocol[cmd]['class']:
            prefix = R
        else:
            prefix = self.protocol[cmd]['class'][0]
        data = "%s%02X" % (prefix, cmd.value)
        raw = self.send(data)
        echo = raw[0:len(data)]
        resp = raw[len(data):]
        if echo == data:
            if self.protocol[cmd]['data'] == hex:   # cast the data
                resp = int(resp, 16)
            elif self.protocol[cmd]['data'] == float:
                resp = float(resp)
            else:
                resp = self.protocol[cmd]['data'] (resp)
        else:
            raise Exception('Invalid Response: %s -> %s' % (raw, self.error_response.get(resp, 'UNKNOWN ERROR')))
        return resp
    def write(self, cmd:Cmd, value):
        if W in self.protocol[cmd]['class']:
            prefix = W
        else:
            prefix = self.protocol[cmd]['class'][0]
        echo = "%s%02X" % (prefix, cmd.value)
        if self.protocol[cmd]['data'] == hex:
            if value >= 0:
                value = abs(round(value))
            else:
                value = abs(round(value)) | (1 << (self.protocol[cmd]['data len'] * 4 - 1))
        formatter = "%%s%%02X%%0%dX" % self.protocol[cmd]['data len']
        data = formatter % (prefix, cmd.value, round(value))
        resp = self.send(data)
        if resp == echo:
            return
        else:
            raise Exception('Invalid Response: %s → %s' % (resp, self.error_response.get(resp, 'UNKNOWN ERROR')))

    def send(self, value):
        resp = super().send(value)
        if self.debug:
            print('%s -> %s' % (value, resp))
        return resp

class MfrError:
    OPEN_UUT_CONN   =   "OPEN_UUT_CONN"



def sound():
    if SOUND_ENABLE ==1:
        while(BEEP.enable):
            winsound.Beep(5440,500)
            time.sleep(1)




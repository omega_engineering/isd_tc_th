import math
import mte, time, datetime

from ISD_TC import *
RevisionLetter='A'
uut = None
dmm = fluke = printer = switchA  = None
model = Model()
CARDA=0
CARDB=1
CARDC=3
# CONST
READING_COUNT = 10
DEBUG = False



DEFAULT_IP_ADDR = "192.168.1.200"
SSL_PORT = 2000

def NotInRangeMSG(stepfailed='step', highlim=10,lowlim=0,reading=100.0):
    return "Failed %s. HIGH LIMIT: %f LO LIMIT :%f READING :%f." %(stepfailed,highlim,lowlim,reading)

class MultiSwitchMaxtrix(SwitchMatrix):
    def open(self, device_class, DeviceID=0):
        self.handle = mte.OpenDevice(device_class, DeviceID)

def AlertOperator(title,msg):
    BEEP.enable = 1
    t = Timer(1, sound)
    t.start()
    Mbox(title, msg, 0)
    BEEP.enable = 0
    t.cancel()


def error(msg):
    AlertOperator("FAILED.", "UUT has failed.")
    switchA.clear()
    mte.Error(msg)



def open_uut():

    global uut
    com = mte.OpenCommunication(mte.Medium.RS232, mte.Protocol.Neutral,
                                mte.SerialChannel(UUT_RS232_COM_PORT, 38400, 8,
                                                  mte.SerialStopBit.ONE, mte.SerialParity.NONE))
    mte.SetTimeout(com, 1000)
    uut = iSeriesDevice(com)
    uut.debug = True

    r=uut.send("") #send carriage return for inital comm
    mte.ReportData(r)
    #r=uut.send("X")
    #mte.ReportData(r)

    return uut


def init_equip():
    global dmm, fluke, printer, switchA
    try:
        mte.ReportData('Opening Printer')
        printer = mte.OpenDevice(mte.Device.Printer)
    except:
        mte.ReportData("Printer not set.")
    mte.ReportData('Opening Fluke')
    fluke = mte.OpenDevice(mte.Device.Omega_CL3001)
    mte.SetVoltage(fluke, 0)
    mte.SetOperateMode(fluke, mte.OperateMode.Standby)
    mte.ReportData('Opening Pickering Card A')
    switchA = MultiSwitchMaxtrix()
    switchA.open(mte.Device.Pickering_Switch_Matrix, CARDA)
    switchA.load(switch_table_pathA)
    switchA.clear()


def test_done():
    print('DONE')
    AlertOperator('PASSED','UUT has passed.')
    switchA.clear()
    mte.Comment("#End Time = %s" % mte.log_time())
    mte.Comment("#Test Status = passed")




def welcome():
    mte.ReportData('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
    mte.ReportData(':  ISD_TC Calibration TP0285    :')
    mte.ReportData(':  Rev %s   ' % RevisionLetter)
    mte.ReportData('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
    mte.ReportData('')
    mte.ReportData('#Product = ISD_TC')
    mte.ReportData('#SW = %s' % SW_REV)
    mte.ReportData("#Start Time = %s" % mte.log_time())


def get_serial_number():
    Second = time.time()
    Second = round(Second)
    Second = Second & 0xFFFFFFFF
    mte.ReportData("Serial number is "+hex(Second))
    return Second

def testTC():
    r=uut.send("0")
    mte.ReportData(r)
    voltages=[
                {
                    "name" : "TC 0mV",
                    "highlim": 10,
                    "lowlim" : -10,
                    "voltage" : 0
                },
                {
                    "name" : "TC 60mV",
                    "highlim": 30000,
                    "lowlim" : 20000,
                    "voltage": 60
                },
                {
                    "name" : "TC -10mV",
                    "highlim": 5000,
                    "lowlim" : 4000,
                    "voltage" : 10
                }

    ]
    TCchannels=[ISD_TCSwitch.TC1,ISD_TCSwitch.TC2]
    TCNegative=[ISD_TCSwitch.TC1N,ISD_TCSwitch.TC2N]
    for TCchannel in TCchannels:
        switchA.switch_on(TCchannel)
        for volt in  voltages:
            if volt["name"] == "TC -10mV":
                switchA.switch_off(TCchannel)
                time.sleep(.5)
                negative_switch=TCNegative.pop(0)
                switchA.switch_on(negative_switch)
            mte.ReportData("Calibrating for %s" %volt["name"] )
            mte.SetVoltage(fluke,volt["voltage"])
            time.sleep(5)
            resp=uut.send("G")
            reading= int(resp[:4],16)
            if reading > 0x8000:
                reading = 0x8000-reading
            mte.ReportData(reading)
            if reading > volt["highlim"] or reading < volt["lowlim"]:
                error(NotInRangeMSG(volt['name'],volt["highlim"],volt["lowlim"],reading))
            else:
                mte.ReportData("Passed %s" % (volt['name']))
        switchA.switch_off(negative_switch)

def testQD():

    voltages=[
                {
                    "name" : "QD 0mV",
                    "highlim": 1000,
                    "lowlim" : 0,
                    "voltage" : 0
                },
                {
                    "name" : "QD 1.5V",
                    "highlim": 55000,
                    "lowlim" : 40000,
                    "voltage": 1500
                },

    ]
    TCchannels=[ISD_TCSwitch.COMP1,ISD_TCSwitch.COMP2]
    for TCchannel in TCchannels:
        switchA.switch_on(TCchannel)
        for volt in  voltages:
            mte.ReportData("Calibrating for %s" %volt["name"] )
            mte.SetVoltage(fluke,volt["voltage"])
            time.sleep(5)
            resp=uut.send("G")
            reading= int(resp[:4],16)
            mte.ReportData(reading)
            if reading > volt["highlim"] or reading < volt["lowlim"]:
                error(NotInRangeMSG(volt['name'],volt["highlim"],volt["lowlim"],reading))
            else:
                mte.ReportData("Passed %s" % (volt['name']))
        switchA.switch_off(TCchannel)

    mte.SetOperateMode(fluke, mte.OperateMode.Standby)

def testp2000():
    global uut

    mte.CloseCommunication(uut)

    time.sleep(5)

    p2000 = mte.OpenCommunication(mte.Medium.TCP, mte.Protocol.Neutral,
                                  mte.TcpChannel(DEFAULT_IP_ADDR, SSL_PORT))

    mte.SetTimeout(p2000, 1000)

    uut=iSeriesDevice(p2000)

    try:
        mte.ReportData("Port 2000 respsonse: " + uut.send("*SRTC"))
    except:
        error("Did not get a response from ethernet connection. "
              "Check ethernet circuit.")



def main():
    welcome()

    init_equip()

    switchA.switch_on(ISD_TCSwitch.POWER)

    time.sleep(2)

    open_uut()

    get_serial_number()

    testTC()

    testQD()

    testp2000()

    test_done()



if __name__ == "__main__":
    main()

